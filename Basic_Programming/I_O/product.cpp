#include <iostream>
using namespace std;
long modulo(long number,long divisor){
	long remainder , quotient;
	quotient = number / divisor;
	remainder = number - ( divisor * quotient ) ;
	return remainder;
}

int main(){
	long product = 1 ;
	long divisor = 1000000007;
	int N , array[1000];
	cin>>N;
	for (int i = 0; i < N; ++i)
		cin>>array[i];
	for (int j = 0; j < N; ++j){
		product = modulo(  (product * array[j]) , divisor );
	}
	cout<<product;
	return 0;
}